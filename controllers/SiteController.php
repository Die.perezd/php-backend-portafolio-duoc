<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;


use app\models\Contacto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $contacto = new Contacto();

        if ($contacto->load(Yii::$app->request->post())) {
            $variables = Yii::$app->request->post()['Contacto'];
            
            $nombre = $variables['nombre'];
            $correoContacto = $variables['correoContacto'];
            $numeroContacto = $variables['numeroContacto'];
            $consulta = $variables['consulta'];
        }

        return $this->render('index',["contacto"=>$contacto]);
    }

    public function actionRegistrame(){
        return $this->render('registrarme');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionConsulta($nombre, $correoContacto, $consulta, $numeroContacto=0){

        $envio = $this->sendMail($correoContacto,$nombre,$consulta,$numeroContacto);
        var_dump("respuesta correo: ".$envio);
        
        return true;
    }

    public static function sendMail($correoContacto,$nombre,$consulta,$numeroContacto){
        var_dump("enviando correo");
        Yii::$app->mailer->compose()
        ->setFrom('pruebas@test.com')
        ->setTo('juan.salgado5320@gmail.com')
        ->setSubject('Prueba Sección Consulta')
        ->setHtmlBody('<h1>Estimado</h1><br><h3>Ha llegado un correo de '.$nombre.', su numero de telefono es '.$numeroContacto.'. Su consulta es la siguiente: </h3><br><p>'.$consulta.'</p>')
        ->send();

        return true;
    }
}
