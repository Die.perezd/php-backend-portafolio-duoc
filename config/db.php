<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=portafolio_duoc',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

//    'class' => 'yii\db\Connection',
//    'dsn' => 'mysql:host=www.db4free.net;dbname=cocinarecetas',
//    'username' => 'cocinarecetas',
//    'password' => 'duoc1234567890',
//    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
