<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EstadoTarea;

/**
 * EstadoTareaSearch represents the model behind the search form of `app\models\EstadoTarea`.
 */
class EstadoTareaSearch extends EstadoTarea
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_estado_tarea'], 'integer'],
            [['nombre_estado', 'descripcion_estado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EstadoTarea::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_estado_tarea' => $this->id_estado_tarea,
        ]);

        $query->andFilterWhere(['like', 'nombre_estado', $this->nombre_estado])
            ->andFilterWhere(['like', 'descripcion_estado', $this->descripcion_estado]);

        return $dataProvider;
    }
}
