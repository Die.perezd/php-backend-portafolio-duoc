<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TareasFlujo;

/**
 * TareasFlujoSearch represents the model behind the search form of `app\models\TareasFlujo`.
 */
class TareasFlujoSearch extends TareasFlujo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tarea_flujo', 'id_flujo', 'id_tarea'], 'integer'],
            [['plazo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TareasFlujo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tarea_flujo' => $this->id_tarea_flujo,
            'id_flujo' => $this->id_flujo,
            'id_tarea' => $this->id_tarea,
        ]);

        $query->andFilterWhere(['like', 'plazo', $this->plazo]);

        return $dataProvider;
    }
}
