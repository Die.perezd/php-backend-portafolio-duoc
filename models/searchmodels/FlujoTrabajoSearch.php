<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FlujoTrabajo;

/**
 * FlujoTrabajoSearch represents the model behind the search form of `app\models\FlujoTrabajo`.
 */
class FlujoTrabajoSearch extends FlujoTrabajo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_flujo', 'id_empresa'], 'integer'],
            [['nombre_flujo', 'descripcion_flujo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlujoTrabajo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_flujo' => $this->id_flujo,
            'id_empresa' => $this->id_empresa,
        ]);

        $query->andFilterWhere(['like', 'nombre_flujo', $this->nombre_flujo])
            ->andFilterWhere(['like', 'descripcion_flujo', $this->descripcion_flujo]);

        return $dataProvider;
    }
}
