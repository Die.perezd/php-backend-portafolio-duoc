<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Unidad;

/**
 * UnidadSearch represents the model behind the search form of `app\models\Unidad`.
 */
class UnidadSearch extends Unidad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_unidad'], 'integer'],
            [['nombre_unidad', 'descripcion_unidad'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Unidad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_unidad' => $this->id_unidad,
        ]);

        $query->andFilterWhere(['like', 'nombre_unidad', $this->nombre_unidad])
            ->andFilterWhere(['like', 'descripcion_unidad', $this->descripcion_unidad]);

        return $dataProvider;
    }
}
