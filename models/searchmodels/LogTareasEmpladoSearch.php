<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TareasEmpleado;

/**
 * LogTareasEmpladoSearch represents the model behind the search form of `app\models\TareasEmpleado`.
 */
class LogTareasEmpladoSearch extends TareasEmpleado
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tarea_empleado_empresa', 'id_empleado_empresa', 'id_empleado_subordinador', 'id_tarea_flujo', 'id_estado_tarea'], 'integer'],
            [['fecha_asignacion', 'fecha_inicio', 'fecha_termino'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TareasEmpleado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tarea_empleado_empresa' => $this->id_tarea_empleado_empresa,
            'id_empleado_empresa' => $this->id_empleado_empresa,
            'id_empleado_subordinador' => $this->id_empleado_subordinador,
            'id_tarea_flujo' => $this->id_tarea_flujo,
            'id_estado_tarea' => $this->id_estado_tarea,
            'fecha_asignacion' => $this->fecha_asignacion,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_termino' => $this->fecha_termino,
        ]);

        return $dataProvider;
    }
}
