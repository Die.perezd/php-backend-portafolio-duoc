<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogTareaEmpleado;

/**
 * LogTareaEmpleadoSearch represents the model behind the search form of `app\models\LogTareaEmpleado`.
 */
class LogTareaEmpleadoSearch extends LogTareaEmpleado
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_log_tarea_empleado', 'id_tarea_empleado_empresa', 'id_estado_tarea'], 'integer'],
            [['fecha_cambio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogTareaEmpleado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_log_tarea_empleado' => $this->id_log_tarea_empleado,
            'fecha_cambio' => $this->fecha_cambio,
            'id_tarea_empleado_empresa' => $this->id_tarea_empleado_empresa,
            'id_estado_tarea' => $this->id_estado_tarea,
        ]);

        return $dataProvider;
    }
}
