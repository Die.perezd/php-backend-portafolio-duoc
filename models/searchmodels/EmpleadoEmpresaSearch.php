<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpleadoEmpresa;

/**
 * EmpleadoEmpresaSearch represents the model behind the search form of `app\models\EmpleadoEmpresa`.
 */
class EmpleadoEmpresaSearch extends EmpleadoEmpresa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_empleado_empresa', 'id_empleado', 'id_empresa', 'id_rol', 'id_unidad', 'id_area', 'tareas_asignadas', 'tareas_terminadas'], 'integer'],
            [['username', 'pass'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpleadoEmpresa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_empleado_empresa' => $this->id_empleado_empresa,
            'id_empleado' => $this->id_empleado,
            'id_empresa' => $this->id_empresa,
            'id_rol' => $this->id_rol,
            'id_unidad' => $this->id_unidad,
            'id_area' => $this->id_area,
            'tareas_asignadas' => $this->tareas_asignadas,
            'tareas_terminadas' => $this->tareas_terminadas,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'pass', $this->pass]);

        return $dataProvider;
    }
}
