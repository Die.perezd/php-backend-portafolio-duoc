<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObservacionesTarea;

/**
 * ObservacionesTareaSearch represents the model behind the search form of `app\models\ObservacionesTarea`.
 */
class ObservacionesTareaSearch extends ObservacionesTarea
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_observacion_tarea', 'id_tarea_empleado_empresa'], 'integer'],
            [['justificacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObservacionesTarea::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_observacion_tarea' => $this->id_observacion_tarea,
            'id_tarea_empleado_empresa' => $this->id_tarea_empleado_empresa,
        ]);

        $query->andFilterWhere(['like', 'justificacion', $this->justificacion]);

        return $dataProvider;
    }
}
