<?php

namespace app\models\searchmodels;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GestionEmpresa;

/**
 * GestionEmpresaSearch represents the model behind the search form of `app\models\GestionEmpresa`.
 */
class GestionEmpresaSearch extends GestionEmpresa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_gestion_empresa', 'id_empresa', 'total_empleados', 'total_tareas_asignadas', 'total_tareas_en_proceso', 'total_tareas_terminadas'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GestionEmpresa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_gestion_empresa' => $this->id_gestion_empresa,
            'id_empresa' => $this->id_empresa,
            'total_empleados' => $this->total_empleados,
            'total_tareas_asignadas' => $this->total_tareas_asignadas,
            'total_tareas_en_proceso' => $this->total_tareas_en_proceso,
            'total_tareas_terminadas' => $this->total_tareas_terminadas,
        ]);

        return $dataProvider;
    }
}
