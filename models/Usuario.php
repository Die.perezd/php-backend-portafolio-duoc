<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $idUsuario
 * @property string|null $nombreUsuario
 * @property string|null $email
 * @property string|null $fechaCreacion
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaCreacion'], 'safe'],
            [['nombreUsuario', 'email'], 'string', 'max' => 45],
            [['nombreUsuario'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'nombreUsuario' => 'Nombre Usuario',
            'email' => 'Email',
            'fechaCreacion' => 'Fecha Creacion',
        ];
    }
}
