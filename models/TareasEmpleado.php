<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tareas_empleado".
 *
 * @property int $id_tarea_empleado_empresa
 * @property int $id_empleado_empresa
 * @property int $id_empleado_subordinador
 * @property int $id_tarea_flujo
 * @property int $id_estado_tarea
 * @property string $fecha_asignacion
 * @property string|null $fecha_inicio
 * @property string|null $fecha_termino
 *
 * @property LogTareaEmpleado[] $logTareaEmpleados
 * @property ObservacionesTarea[] $observacionesTareas
 * @property EmpleadoEmpresa $empleadoEmpresa
 * @property EmpleadoEmpresa $empleadoSubordinador
 * @property EstadoTarea $estadoTarea
 * @property TareasFlujo $tareaFlujo
 */
class TareasEmpleado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tareas_empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_empleado_empresa', 'id_empleado_subordinador', 'id_tarea_flujo', 'id_estado_tarea', 'fecha_asignacion'], 'required'],
            [['id_empleado_empresa', 'id_empleado_subordinador', 'id_tarea_flujo', 'id_estado_tarea'], 'integer'],
            [['fecha_asignacion', 'fecha_inicio', 'fecha_termino'], 'safe'],
            [['id_empleado_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => EmpleadoEmpresa::className(), 'targetAttribute' => ['id_empleado_empresa' => 'id_empleado_empresa']],
            [['id_empleado_subordinador'], 'exist', 'skipOnError' => true, 'targetClass' => EmpleadoEmpresa::className(), 'targetAttribute' => ['id_empleado_subordinador' => 'id_empleado_empresa']],
            [['id_estado_tarea'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoTarea::className(), 'targetAttribute' => ['id_estado_tarea' => 'id_estado_tarea']],
            [['id_tarea_flujo'], 'exist', 'skipOnError' => true, 'targetClass' => TareasFlujo::className(), 'targetAttribute' => ['id_tarea_flujo' => 'id_tarea_flujo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tarea_empleado_empresa' => 'Id Tarea Empleado Empresa',
            'id_empleado_empresa' => 'Id Empleado Empresa',
            'id_empleado_subordinador' => 'Id Empleado Subordinador',
            'id_tarea_flujo' => 'Id Tarea Flujo',
            'id_estado_tarea' => 'Id Estado Tarea',
            'fecha_asignacion' => 'Fecha Asignacion',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_termino' => 'Fecha Termino',
        ];
    }

    /**
     * Gets query for [[LogTareaEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogTareaEmpleados()
    {
        return $this->hasMany(LogTareaEmpleado::className(), ['id_tarea_empleado_empresa' => 'id_tarea_empleado_empresa']);
    }

    /**
     * Gets query for [[ObservacionesTareas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObservacionesTareas()
    {
        return $this->hasMany(ObservacionesTarea::className(), ['id_tarea_empleado_empresa' => 'id_tarea_empleado_empresa']);
    }

    /**
     * Gets query for [[EmpleadoEmpresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEmpresa()
    {
        return $this->hasOne(EmpleadoEmpresa::className(), ['id_empleado_empresa' => 'id_empleado_empresa']);
    }

    /**
     * Gets query for [[EmpleadoSubordinador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoSubordinador()
    {
        return $this->hasOne(EmpleadoEmpresa::className(), ['id_empleado_empresa' => 'id_empleado_subordinador']);
    }

    /**
     * Gets query for [[EstadoTarea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoTarea()
    {
        return $this->hasOne(EstadoTarea::className(), ['id_estado_tarea' => 'id_estado_tarea']);
    }

    /**
     * Gets query for [[TareaFlujo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareaFlujo()
    {
        return $this->hasOne(TareasFlujo::className(), ['id_tarea_flujo' => 'id_tarea_flujo']);
    }
}
