<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gestion_empresa".
 *
 * @property int $id_gestion_empresa
 * @property int $id_empresa
 * @property int|null $total_empleados
 * @property int|null $total_tareas_asignadas
 * @property int|null $total_tareas_en_proceso
 * @property int|null $total_tareas_terminadas
 *
 * @property Empresa $empresa
 */
class GestionEmpresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gestion_empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_empresa'], 'required'],
            [['id_empresa', 'total_empleados', 'total_tareas_asignadas', 'total_tareas_en_proceso', 'total_tareas_terminadas'], 'integer'],
            [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['id_empresa' => 'id_empresa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_gestion_empresa' => 'Id Gestion Empresa',
            'id_empresa' => 'Id Empresa',
            'total_empleados' => 'Total Empleados',
            'total_tareas_asignadas' => 'Total Tareas Asignadas',
            'total_tareas_en_proceso' => 'Total Tareas En Proceso',
            'total_tareas_terminadas' => 'Total Tareas Terminadas',
        ];
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id_empresa' => 'id_empresa']);
    }
}
