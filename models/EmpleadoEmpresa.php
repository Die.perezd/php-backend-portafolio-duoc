<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleado_empresa".
 *
 * @property int $id_empleado_empresa
 * @property int $id_empleado
 * @property int $id_empresa
 * @property int $id_rol
 * @property int $id_unidad
 * @property int $id_area
 * @property int|null $tareas_asignadas
 * @property int|null $tareas_terminadas
 * @property string $username
 * @property string $pass
 *
 * @property Area $area
 * @property Empleado $empleado
 * @property Empresa $empresa
 * @property Rol $rol
 * @property Unidad $unidad
 * @property TareasEmpleado[] $tareasEmpleados
 * @property TareasEmpleado[] $tareasEmpleados0
 */
class EmpleadoEmpresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleado_empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_empleado', 'id_empresa', 'id_rol', 'id_unidad', 'id_area'], 'required'],
            [['id_empleado', 'id_empresa', 'id_rol', 'id_unidad', 'id_area', 'tareas_asignadas', 'tareas_terminadas'], 'integer'],
            [['pass'], 'string'],
            [['username'], 'string', 'max' => 60],
            [['id_area'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['id_area' => 'id_area']],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['id_empleado' => 'id_empleado']],
            [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['id_empresa' => 'id_empresa']],
            [['id_rol'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['id_rol' => 'id_rol']],
            [['id_unidad'], 'exist', 'skipOnError' => true, 'targetClass' => Unidad::className(), 'targetAttribute' => ['id_unidad' => 'id_unidad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_empleado_empresa' => 'Id Empleado Empresa',
            'id_empleado' => 'Id Empleado',
            'id_empresa' => 'Id Empresa',
            'id_rol' => 'Id Rol',
            'id_unidad' => 'Id Unidad',
            'id_area' => 'Id Area',
            'tareas_asignadas' => 'Tareas Asignadas',
            'tareas_terminadas' => 'Tareas Terminadas',
            'username' => 'Username',
            'pass' => 'Pass',
        ];
    }

    /**
     * Gets query for [[Area]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id_area' => 'id_area']);
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleado::className(), ['id_empleado' => 'id_empleado']);
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id_empresa' => 'id_empresa']);
    }

    /**
     * Gets query for [[Rol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(Rol::className(), ['id_rol' => 'id_rol']);
    }

    /**
     * Gets query for [[Unidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnidad()
    {
        return $this->hasOne(Unidad::className(), ['id_unidad' => 'id_unidad']);
    }

    /**
     * Gets query for [[TareasEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareasEmpleados()
    {
        return $this->hasMany(TareasEmpleado::className(), ['id_empleado_empresa' => 'id_empleado_empresa']);
    }

    /**
     * Gets query for [[TareasEmpleados0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareasEmpleados0()
    {
        return $this->hasMany(TareasEmpleado::className(), ['id_empleado_subordinador' => 'id_empleado_empresa']);
    }
}
