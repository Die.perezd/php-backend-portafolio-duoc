<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property int $id_area
 * @property string $nombre_area
 * @property string|null $decripcion_area
 *
 * @property EmpleadoEmpresa[] $empleadoEmpresas
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_area'], 'required'],
            [['nombre_area'], 'string', 'max' => 40],
            [['decripcion_area'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_area' => 'Id Área',
            'nombre_area' => 'Nombre Área',
            'decripcion_area' => 'Decripción Área',
        ];
    }

    /**
     * Gets query for [[EmpleadoEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEmpresas()
    {
        return $this->hasMany(EmpleadoEmpresa::className(), ['id_area' => 'id_area']);
    }
}
