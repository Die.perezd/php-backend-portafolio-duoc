<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_tarea".
 *
 * @property int $id_estado_tarea
 * @property string $nombre_estado
 * @property string|null $descripcion_estado
 *
 * @property LogTareaEmpleado[] $logTareaEmpleados
 * @property TareasEmpleado[] $tareasEmpleados
 */
class EstadoTarea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_tarea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_estado'], 'required'],
            [['nombre_estado'], 'string', 'max' => 40],
            [['descripcion_estado'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_estado_tarea' => 'Id Estado Tarea',
            'nombre_estado' => 'Nombre Estado',
            'descripcion_estado' => 'Descripcion Estado',
        ];
    }

    /**
     * Gets query for [[LogTareaEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogTareaEmpleados()
    {
        return $this->hasMany(LogTareaEmpleado::className(), ['id_estado_tarea' => 'id_estado_tarea']);
    }

    /**
     * Gets query for [[TareasEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareasEmpleados()
    {
        return $this->hasMany(TareasEmpleado::className(), ['id_estado_tarea' => 'id_estado_tarea']);
    }
}
