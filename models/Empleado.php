<?php

namespace app\models;

use phpDocumentor\Reflection\Types\This;
use Yii;

/**
 * This is the model class for table "empleado".
 *
 * @property int $id_empleado
 * @property string $nombres
 * @property string $primer_apellido
 * @property string|null $segundo_apellido
 * @property string|null $numero_documento
 * @property string|null $fecha_nacimiento
 * @property string|null $direccion
 * @property string|null $email
 *
 * @property EmpleadoEmpresa[] $empleadoEmpresas
 */
class Empleado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombres', 'primer_apellido'], 'required'],
            [['fecha_nacimiento'], 'safe'],
            [['nombres', 'direccion', 'email'], 'string', 'max' => 100],
            [['primer_apellido', 'segundo_apellido'], 'string', 'max' => 60],
            [['numero_documento'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_empleado' => 'Id Empleado',
            'nombres' => 'Nombres',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'numero_documento' => 'Numero Documento',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'direccion' => 'Direccion',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[EmpleadoEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEmpresas()
    {
        return $this->hasMany(EmpleadoEmpresa::className(), ['id_empleado' => 'id_empleado']);
    }

    public function getFullName()
    {
        return $this->nombres . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }
}
