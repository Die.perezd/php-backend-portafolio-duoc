<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tareas_flujo".
 *
 * @property int $id_tarea_flujo
 * @property int|null $id_flujo
 * @property int $id_tarea
 * @property string|null $plazo
 *
 * @property TareasEmpleado[] $tareasEmpleados
 * @property FlujoTrabajo $flujo
 * @property Tarea $tarea
 */
class TareasFlujo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tareas_flujo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_flujo', 'id_tarea'], 'integer'],
            [['id_tarea'], 'required'],
            [['plazo'], 'string', 'max' => 40],
            [['id_flujo'], 'exist', 'skipOnError' => true, 'targetClass' => FlujoTrabajo::className(), 'targetAttribute' => ['id_flujo' => 'id_flujo']],
            [['id_tarea'], 'exist', 'skipOnError' => true, 'targetClass' => Tarea::className(), 'targetAttribute' => ['id_tarea' => 'id_tarea']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tarea_flujo' => 'Id Tarea Flujo',
            'id_flujo' => 'Id Flujo',
            'id_tarea' => 'Id Tarea',
            'plazo' => 'Plazo',
        ];
    }

    /**
     * Gets query for [[TareasEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareasEmpleados()
    {
        return $this->hasMany(TareasEmpleado::className(), ['id_tarea_flujo' => 'id_tarea_flujo']);
    }

    /**
     * Gets query for [[Flujo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlujo()
    {
        return $this->hasOne(FlujoTrabajo::className(), ['id_flujo' => 'id_flujo']);
    }

    /**
     * Gets query for [[Tarea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarea()
    {
        return $this->hasOne(Tarea::className(), ['id_tarea' => 'id_tarea']);
    }
}
