<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_tarea_empleado".
 *
 * @property int $id_log_tarea_empleado
 * @property string|null $fecha_cambio
 * @property int $id_tarea_empleado_empresa
 * @property int $id_estado_tarea
 *
 * @property EstadoTarea $estadoTarea
 * @property TareasEmpleado $tareaEmpleadoEmpresa
 */
class LogTareaEmpleado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_tarea_empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_cambio'], 'safe'],
            [['id_tarea_empleado_empresa', 'id_estado_tarea'], 'required'],
            [['id_tarea_empleado_empresa', 'id_estado_tarea'], 'integer'],
            [['id_estado_tarea'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoTarea::className(), 'targetAttribute' => ['id_estado_tarea' => 'id_estado_tarea']],
            [['id_tarea_empleado_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => TareasEmpleado::className(), 'targetAttribute' => ['id_tarea_empleado_empresa' => 'id_tarea_empleado_empresa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_log_tarea_empleado' => 'Id Log Tarea Empleado',
            'fecha_cambio' => 'Fecha Cambio',
            'id_tarea_empleado_empresa' => 'Id Tarea Empleado Empresa',
            'id_estado_tarea' => 'Id Estado Tarea',
        ];
    }

    /**
     * Gets query for [[EstadoTarea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoTarea()
    {
        return $this->hasOne(EstadoTarea::className(), ['id_estado_tarea' => 'id_estado_tarea']);
    }

    /**
     * Gets query for [[TareaEmpleadoEmpresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareaEmpleadoEmpresa()
    {
        return $this->hasOne(TareasEmpleado::className(), ['id_tarea_empleado_empresa' => 'id_tarea_empleado_empresa']);
    }
}
