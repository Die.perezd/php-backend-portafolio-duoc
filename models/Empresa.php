<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $id_empresa
 * @property string $nombre_empresa
 * @property string|null $rubro
 * @property string|null $direccion
 * @property int|null $telefono
 * @property string|null $fecha_registro
 * @property string|null $estado
 *
 * @property EmpleadoEmpresa[] $empleadoEmpresas
 * @property FlujoTrabajo[] $flujoTrabajos
 * @property GestionEmpresa[] $gestionEmpresas
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_empresa'], 'required'],
            [['telefono'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['nombre_empresa'], 'string', 'max' => 100],
            [['rubro', 'estado'], 'string', 'max' => 40],
            [['direccion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_empresa' => 'Id Empresa',
            'nombre_empresa' => 'Nombre Empresa',
            'rubro' => 'Rubro',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'fecha_registro' => 'Fecha Registro',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[EmpleadoEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEmpresas()
    {
        return $this->hasMany(EmpleadoEmpresa::className(), ['id_empresa' => 'id_empresa']);
    }

    /**
     * Gets query for [[FlujoTrabajos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlujoTrabajos()
    {
        return $this->hasMany(FlujoTrabajo::className(), ['id_empresa' => 'id_empresa']);
    }

    /**
     * Gets query for [[GestionEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGestionEmpresas()
    {
        return $this->hasMany(GestionEmpresa::className(), ['id_empresa' => 'id_empresa']);
    }
}
