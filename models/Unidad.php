<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unidad".
 *
 * @property int $id_unidad
 * @property string $nombre_unidad
 * @property string|null $descripcion_unidad
 *
 * @property EmpleadoEmpresa[] $empleadoEmpresas
 */
class Unidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_unidad'], 'required'],
            [['nombre_unidad'], 'string', 'max' => 40],
            [['descripcion_unidad'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_unidad' => 'Id Unidad',
            'nombre_unidad' => 'Nombre Unidad',
            'descripcion_unidad' => 'Descripcion Unidad',
        ];
    }

    /**
     * Gets query for [[EmpleadoEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEmpresas()
    {
        return $this->hasMany(EmpleadoEmpresa::className(), ['id_unidad' => 'id_unidad']);
    }
}
