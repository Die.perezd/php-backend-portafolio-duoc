<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rol".
 *
 * @property int $id_rol
 * @property string $nombre_rol
 * @property string|null $descripcion_rol
 *
 * @property EmpleadoEmpresa[] $empleadoEmpresas
 */
class Rol extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_rol'], 'required'],
            [['nombre_rol'], 'string', 'max' => 40],
            [['descripcion_rol'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_rol' => 'Id Rol',
            'nombre_rol' => 'Nombre Rol',
            'descripcion_rol' => 'Descripcion Rol',
        ];
    }

    /**
     * Gets query for [[EmpleadoEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleadoEmpresas()
    {
        return $this->hasMany(EmpleadoEmpresa::className(), ['id_rol' => 'id_rol']);
    }
}
