<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flujo_trabajo".
 *
 * @property int $id_flujo
 * @property int $id_empresa
 * @property string $nombre_flujo
 * @property string|null $descripcion_flujo
 *
 * @property Empresa $empresa
 * @property TareasFlujo[] $tareasFlujos
 */
class FlujoTrabajo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flujo_trabajo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_empresa', 'nombre_flujo'], 'required'],
            [['id_empresa'], 'integer'],
            [['nombre_flujo'], 'string', 'max' => 40],
            [['descripcion_flujo'], 'string', 'max' => 255],
            [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['id_empresa' => 'id_empresa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_flujo' => 'Id Flujo',
            'id_empresa' => 'Id Empresa',
            'nombre_flujo' => 'Nombre Flujo',
            'descripcion_flujo' => 'Descripcion Flujo',
        ];
    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id_empresa' => 'id_empresa']);
    }

    /**
     * Gets query for [[TareasFlujos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareasFlujos()
    {
        return $this->hasMany(TareasFlujo::className(), ['id_flujo' => 'id_flujo']);
    }
}
