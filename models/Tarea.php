<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarea".
 *
 * @property int $id_tarea
 * @property string $nombre_tarea
 * @property string|null $descripcion_tarea
 *
 * @property TareasFlujo[] $tareasFlujos
 */
class Tarea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_tarea'], 'required'],
            [['nombre_tarea'], 'string', 'max' => 40],
            [['descripcion_tarea'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tarea' => 'Id Tarea',
            'nombre_tarea' => 'Nombre Tarea',
            'descripcion_tarea' => 'Descripcion Tarea',
        ];
    }

    /**
     * Gets query for [[TareasFlujos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareasFlujos()
    {
        return $this->hasMany(TareasFlujo::className(), ['id_tarea' => 'id_tarea']);
    }
}
