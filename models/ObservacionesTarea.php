<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "observaciones_tarea".
 *
 * @property int $id_observacion_tarea
 * @property int $id_tarea_empleado_empresa
 * @property string|null $justificacion
 *
 * @property TareasEmpleado $tareaEmpleadoEmpresa
 */
class ObservacionesTarea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'observaciones_tarea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tarea_empleado_empresa'], 'required'],
            [['id_tarea_empleado_empresa'], 'integer'],
            [['justificacion'], 'string'],
            [['id_tarea_empleado_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => TareasEmpleado::className(), 'targetAttribute' => ['id_tarea_empleado_empresa' => 'id_tarea_empleado_empresa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_observacion_tarea' => 'Id Observacion Tarea',
            'id_tarea_empleado_empresa' => 'Id Tarea Empleado Empresa',
            'justificacion' => 'Justificacion',
        ];
    }

    /**
     * Gets query for [[TareaEmpleadoEmpresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareaEmpleadoEmpresa()
    {
        return $this->hasOne(TareasEmpleado::className(), ['id_tarea_empleado_empresa' => 'id_tarea_empleado_empresa']);
    }
}
