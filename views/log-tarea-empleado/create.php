<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogTareaEmpleado */

$this->title = 'Create Log Tarea Empleado';
$this->params['breadcrumbs'][] = ['label' => 'Log Tarea Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-tarea-empleado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
