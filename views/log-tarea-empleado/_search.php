<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchmodels\LogTareaEmpleadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-tarea-empleado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_log_tarea_empleado') ?>

    <?= $form->field($model, 'fecha_cambio') ?>

    <?= $form->field($model, 'id_tarea_empleado_empresa') ?>

    <?= $form->field($model, 'id_estado_tarea') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
