<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogTareaEmpleado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-tarea-empleado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha_cambio')->textInput() ?>

    <?= $form->field($model, 'id_tarea_empleado_empresa')->textInput() ?>

    <?= $form->field($model, 'id_estado_tarea')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
