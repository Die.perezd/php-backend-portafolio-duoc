<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogTareaEmpleado */

$this->title = 'Update Log Tarea Empleado: ' . $model->id_log_tarea_empleado;
$this->params['breadcrumbs'][] = ['label' => 'Log Tarea Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_log_tarea_empleado, 'url' => ['view', 'id' => $model->id_log_tarea_empleado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-tarea-empleado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
