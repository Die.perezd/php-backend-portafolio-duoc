<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GestionEmpresa */

$this->title = 'Update Gestion Empresa: ' . $model->id_gestion_empresa;
$this->params['breadcrumbs'][] = ['label' => 'Gestion Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_gestion_empresa, 'url' => ['view', 'id' => $model->id_gestion_empresa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gestion-empresa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
