<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GestionEmpresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gestion-empresa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_empresa')->textInput() ?>

    <?= $form->field($model, 'total_empleados')->textInput() ?>

    <?= $form->field($model, 'total_tareas_asignadas')->textInput() ?>

    <?= $form->field($model, 'total_tareas_en_proceso')->textInput() ?>

    <?= $form->field($model, 'total_tareas_terminadas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
