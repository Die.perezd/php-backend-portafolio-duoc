<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GestionEmpresa */

$this->title = 'Create Gestion Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Gestion Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gestion-empresa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
