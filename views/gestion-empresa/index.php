<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\searchmodels\GestionEmpresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gestion Empresas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gestion-empresa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Gestion Empresa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_gestion_empresa',
            'id_empresa',
            'total_empleados',
            'total_tareas_asignadas',
            'total_tareas_en_proceso',
            //'total_tareas_terminadas',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
