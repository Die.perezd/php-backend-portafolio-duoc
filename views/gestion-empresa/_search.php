<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchmodels\GestionEmpresaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gestion-empresa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_gestion_empresa') ?>

    <?= $form->field($model, 'id_empresa') ?>

    <?= $form->field($model, 'total_empleados') ?>

    <?= $form->field($model, 'total_tareas_asignadas') ?>

    <?= $form->field($model, 'total_tareas_en_proceso') ?>

    <?php // echo $form->field($model, 'total_tareas_terminadas') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
