<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TareasEmpleado */

$this->title = $model->id_tarea_empleado_empresa;
$this->params['breadcrumbs'][] = ['label' => 'Tareas Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tareas-empleado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_tarea_empleado_empresa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_tarea_empleado_empresa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_tarea_empleado_empresa',
            'id_empleado_empresa',
            'id_empleado_subordinador',
            'id_tarea_flujo',
            'id_estado_tarea',
            'fecha_asignacion',
            'fecha_inicio',
            'fecha_termino',
        ],
    ]) ?>

</div>
