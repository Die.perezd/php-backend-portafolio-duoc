<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchmodels\LogTareasEmpladoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tareas-empleado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_tarea_empleado_empresa') ?>

    <?= $form->field($model, 'id_empleado_empresa') ?>

    <?= $form->field($model, 'id_empleado_subordinador') ?>

    <?= $form->field($model, 'id_tarea_flujo') ?>

    <?= $form->field($model, 'id_estado_tarea') ?>

    <?php // echo $form->field($model, 'fecha_asignacion') ?>

    <?php // echo $form->field($model, 'fecha_inicio') ?>

    <?php // echo $form->field($model, 'fecha_termino') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
