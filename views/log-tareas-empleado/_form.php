<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TareasEmpleado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tareas-empleado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_empleado_empresa')->textInput() ?>

    <?= $form->field($model, 'id_empleado_subordinador')->textInput() ?>

    <?= $form->field($model, 'id_tarea_flujo')->textInput() ?>

    <?= $form->field($model, 'id_estado_tarea')->textInput() ?>

    <?= $form->field($model, 'fecha_asignacion')->textInput() ?>

    <?= $form->field($model, 'fecha_inicio')->textInput() ?>

    <?= $form->field($model, 'fecha_termino')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
