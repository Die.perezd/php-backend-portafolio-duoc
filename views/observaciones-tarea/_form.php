<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObservacionesTarea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="observaciones-tarea-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tarea_empleado_empresa')->textInput() ?>

    <?= $form->field($model, 'justificacion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
