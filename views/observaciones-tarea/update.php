<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObservacionesTarea */

$this->title = 'Update Observaciones Tarea: ' . $model->id_observacion_tarea;
$this->params['breadcrumbs'][] = ['label' => 'Observaciones Tareas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_observacion_tarea, 'url' => ['view', 'id' => $model->id_observacion_tarea]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="observaciones-tarea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
