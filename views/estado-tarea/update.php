<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoTarea */

$this->title = 'Update Estado Tarea: ' . $model->id_estado_tarea;
$this->params['breadcrumbs'][] = ['label' => 'Estado Tareas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_estado_tarea, 'url' => ['view', 'id' => $model->id_estado_tarea]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estado-tarea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
