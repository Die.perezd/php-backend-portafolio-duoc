<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoTarea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-tarea-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_estado')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
