<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadoEmpresa */

$this->title = 'Create Empleado Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Empleado Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-empresa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
