<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadoEmpresa */

$this->title = 'Update Empleado Empresa: ' . $model->id_empleado_empresa;
$this->params['breadcrumbs'][] = ['label' => 'Empleado Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_empleado_empresa, 'url' => ['view', 'id' => $model->id_empleado_empresa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empleado-empresa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
