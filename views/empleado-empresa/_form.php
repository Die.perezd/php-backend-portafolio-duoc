<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadoEmpresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-empresa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_empleado')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(app\models\Empleado::find()->all(), 'id_empleado', 'fullName'),
        'options' => [
            'placeholder' => 'Seleccione un empleado',
        ]
    ]) ?>

    <?= $form->field($model, 'id_empresa')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(app\models\Empresa::find()->all(), 'id_empresa', 'nombre_empresa'),
        'options' => [
            'placeholder' => 'Seleccione una empresa',
        ]
    ]) ?>

    <?= $form->field($model, 'id_rol')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(app\models\Rol::find()->all(), 'id_rol', 'nombre_rol'),
        'options' => [
            'placeholder' => 'Seleccione un rol',
        ]
    ]) ?>


    <?= $form->field($model, 'id_unidad')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(app\models\Unidad::find()->all(), 'id_unidad', 'nombre_unidad'),
        'options' => [
            'placeholder' => 'Seleccione una unidad',
        ]
    ]) ?>

    <?= $form->field($model, 'id_area')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(app\models\Area::find()->all(), 'id_area', 'nombre_area'),
        'options' => [
            'placeholder' => 'Seleccione un área',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
