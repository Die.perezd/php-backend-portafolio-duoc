<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchmodels\EmpleadoEmpresaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-empresa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_empleado_empresa') ?>

    <?= $form->field($model, 'id_empleado') ?>

    <?= $form->field($model, 'id_empresa') ?>

    <?= $form->field($model, 'id_rol') ?>

    <?= $form->field($model, 'id_unidad') ?>

    <?php // echo $form->field($model, 'id_area') ?>

    <?php // echo $form->field($model, 'tareas_asignadas') ?>

    <?php // echo $form->field($model, 'tareas_terminadas') ?>

    <?php // echo $form->field($model, 'username') ?>

    <?php // echo $form->field($model, 'pass') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
