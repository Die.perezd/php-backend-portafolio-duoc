<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadoEmpresa */

$this->title = $model->id_empleado_empresa;
$this->params['breadcrumbs'][] = ['label' => 'Empleado Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="empleado-empresa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_empleado_empresa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_empleado_empresa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_empleado_empresa',
            'id_empleado',
            'id_empresa',
            'id_rol',
            'id_unidad',
            'id_area',
            'tareas_asignadas',
            'tareas_terminadas',
            'username',
            'pass:ntext',
        ],
    ]) ?>

</div>
