<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchmodels\TareasEmpleadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tareas Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tareas-empleado-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tareas Empleado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tarea_empleado_empresa',
            'id_empleado_empresa',
            'id_empleado_subordinador',
            'id_tarea_flujo',
            'id_estado_tarea',
            //'fecha_asignacion',
            //'fecha_inicio',
            //'fecha_termino',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
