<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TareasEmpleado */

$this->title = 'Update Tareas Empleado: ' . $model->id_tarea_empleado_empresa;
$this->params['breadcrumbs'][] = ['label' => 'Tareas Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tarea_empleado_empresa, 'url' => ['view', 'id' => $model->id_tarea_empleado_empresa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tareas-empleado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
