<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'A.S. Imprenta';
?>


<!--<div class="col-xs-12 divContacto form-group">-->
<!--    <form class="col-xs-12 col-md-8">-->
<!--        <label>Nombre</label>-->
<!--        <input class="form-control" id="nombre" required="true" type="text" name="nombre">-->
<!--        <br>-->
<!--        <label>Correo</label>-->
<!--        <input class="form-control" id="correoContacto" required="true" type="email" name="correoContacto">-->
<!--        <br>-->
<!--        <label>Número Contacto</label>-->
<!--        <input class="form-control" id="numeroContacto" type="text" name="numeroContacto">-->
<!--        <br>-->
<!--        <label>Consulta</label>-->
<!--        <textarea id="consulta" rows="10" maxlength="1500" placeholder="Escribenos tu Consulta" required="true" class="form-control" name="consulta"></textarea>-->
<!--        <br>-->
<!--        <button class="btn btn-success" id="enviar">Enviar</button>-->
<!--    </form>-->
<!--</div>-->

<div class="col-xs-12">
    <h1 class="text-center">Portafolio 2021</h1>
    <br><br><br><br>
    <h4>Grupo de desarrollo:</h4>
    <ul>
        <li><h4>Diego Pérez</h4></li>
        <li><h4>Maximiliano Persolja</h4></li>
        <li><h4>Juan Salgado</h4></li>
    </ul>
</div>

<style>
    .divContacto {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    textarea {
        resize: none;
    }
</style>

<script>

    $(function () {
        $('#enviar').click(function (e) {
            e.preventDefault();

            postData = {
                "nombre": $("#nombre").val(),
                "correoContacto": $("#correoContacto").val(),
                "consulta": $("#consulta").val(),
                "numeroContacto": $("#numeroContacto").val()
            }

            $.get("index.php?r=site/consulta", postData, function (response) {
                if (response) {
                    alert("Correo enviado exitosamente");
                } else {
                    alert("Ha ocurrido un error al intentar enviar el correo.")
                }
            })
        });
    });
</script>