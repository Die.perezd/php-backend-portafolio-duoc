<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TareasFlujo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tareas-flujo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_flujo')->textInput() ?>

    <?= $form->field($model, 'id_tarea')->textInput() ?>

    <?= $form->field($model, 'plazo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
