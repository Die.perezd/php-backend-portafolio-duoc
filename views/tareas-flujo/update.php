<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TareasFlujo */

$this->title = 'Update Tareas Flujo: ' . $model->id_tarea_flujo;
$this->params['breadcrumbs'][] = ['label' => 'Tareas Flujos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tarea_flujo, 'url' => ['view', 'id' => $model->id_tarea_flujo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tareas-flujo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
