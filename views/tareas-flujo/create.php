<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TareasFlujo */

$this->title = 'Create Tareas Flujo';
$this->params['breadcrumbs'][] = ['label' => 'Tareas Flujos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tareas-flujo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
