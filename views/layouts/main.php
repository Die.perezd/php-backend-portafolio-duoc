<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use http\Env\Request;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- Archivos externos -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Portafolio Duoc UC 2021</a>
            </div>
            <ul class="nav navbar-nav">
                <li>
                    <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Administración
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li class="dropdown-header">Personas</li>
                        <li><?= Html::a('Colaboradores', ['empleado/index']) ?></li>
                        <li role="separator" class="divider"></li>

                        <li class="dropdown-header">Organización</li>
                        <li><?= Html::a('Empresas', ['empresa/index']) ?></li>
                        <li><?= Html::a('Áreas', ['area/index']) ?></li>
                        <li><?= Html::a('Roles', ['rol/index']) ?></a></li>
                        <li><?= Html::a('Unidades', ['unidad/index']) ?></a></li>
                        <li><?= Html::a('Colaboradores Empresa', ['empleado-empresa/index']) ?></a></li>
                        <li role="separator" class="divider"></li>

                        <li class="dropdown-header">Tareas y Flujos</li>
                        <li><?= Html::a('Tareas', ['tarea/index']) ?></a></li>
                        <li><?= Html::a('Estados Tareas', ['estado-tarea/index']) ?></a></li>
                        <li><?= Html::a('Flujos de Trabajo', ['flujo-trabajo/index']) ?></a></li>
                        <li><?= Html::a('Tareas Flujo de Trabajo', ['tareas-flujo/index']) ?></a></li>
                        <li role="separator" class="divider"></li>

                        <li class="dropdown-header">Avance</li>
                        <li><?= Html::a('Tareas Empleado', ['tareas-empleado/index']) ?></a></li>
                        <li><?= Html::a('Log Tareas Empleado', ['log-tareas-empleado/index']) ?></a></li>
                        <li><?= Html::a('Observaciones de Tareas', ['observaciones-tarea/index']) ?></a></li>
                        <li><?= Html::a('Gestión de Empresa', ['gestion-empresa/index']) ?></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php?r=site/registrame"><span class="glyphicon glyphicon-user"></span> Registrarme</a></li>
                <li><a href="index.php?r=site/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<style>

    .navbar {
        border-radius: 0px;
    }
</style>