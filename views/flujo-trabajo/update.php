<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlujoTrabajo */

$this->title = 'Update Flujo Trabajo: ' . $model->id_flujo;
$this->params['breadcrumbs'][] = ['label' => 'Flujo Trabajos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_flujo, 'url' => ['view', 'id' => $model->id_flujo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="flujo-trabajo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
