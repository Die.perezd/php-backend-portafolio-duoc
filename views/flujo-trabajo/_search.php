<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchmodels\FlujoTrabajoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flujo-trabajo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_flujo') ?>

    <?= $form->field($model, 'id_empresa') ?>

    <?= $form->field($model, 'nombre_flujo') ?>

    <?= $form->field($model, 'descripcion_flujo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
