<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlujoTrabajo */

$this->title = 'Create Flujo Trabajo';
$this->params['breadcrumbs'][] = ['label' => 'Flujo Trabajos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flujo-trabajo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
